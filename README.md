# Salary calculator

SalaryCalculator is a project which helps calculate monthly salary.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

1. Java 8
2. Installed `docker` and `docker-compose`

### Building

Run following command to build docker images for this project:

***Windows***
```bash
mvnw.cmd clean install
```

***Unix based system***
```bash
./mvnw clean install
```

### Running

Run command to start application (witin project directory):

```bash
docker-compose up
```

Application should start and be exposed on port 8080. Check `/health` endpoint: `http://localhost:8080/health`
You should see following response:
```
200 OK
{
    status: "UP"
}
```

Frontend part should be exposed under port `80`. Access it through browser: `http://localhost`.
