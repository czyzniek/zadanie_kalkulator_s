package com.sonalake.salarycalculator.salary

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static io.restassured.http.ContentType.JSON
import static io.restassured.module.mockmvc.RestAssuredMockMvc.given
import static org.hamcrest.CoreMatchers.equalTo

@WebMvcTest(SalaryController)
class SalaryControllerSpec extends Specification {

    @Autowired
    MockMvc mvc

    @Autowired
    SalaryService salaryService

    def "should calculate salary in PLN"() {
        given:
        def requestBody = """{
            "dailyRate": 100,
            "currencyCode": "EUR"
        }"""
        def request = given()
                .mockMvc(mvc)
                .body(requestBody)
                .contentType(JSON)

        when:
        def response = request.when().post("/salary")

        then:
        response.then()
                .statusCode(200)
                .body("salaryInPLN", equalTo(400.00f))

        then:
        1 * salaryService.calculateSalaryInPLN(100, "EUR") >> 400.00
    }

    def "should return BAD_REQUEST due to wrong currencyCode"() {
        given:
        salaryService.calculateSalaryInPLN(100, "WRONG") >> { throw new IllegalArgumentException("Wrong currency code!") }
        def requestBody = """{
            "dailyRate": 100,
            "currencyCode": "WRONG"
        }"""
        def request = given()
                .mockMvc(mvc)
                .body(requestBody)
                .contentType(JSON)

        when:
        def response = request.when().post("/salary")

        then:
        response.then()
                .statusCode(400)
                .body("error", equalTo("Wrong currency code!"))
    }


    @TestConfiguration
    static class MockConfig {
        def detachedMockFactory = new DetachedMockFactory()

        @Bean
        @Primary
        SalaryService salaryService() {
            return detachedMockFactory.Mock(SalaryService)
        }
    }

}
