package com.sonalake.salarycalculator.nbp

import spock.lang.Specification

class NBPClientSpec extends Specification {

    def sut = new NBPClient()

    def "should get list of exchange rates from table A"() {
        when:
        def result = sut.getExchangeRatesTables()

        then:
        !result.isEmpty()
        with(result[0]) {
            table == "A"
            !rates.isEmpty()
        }
    }
}
