package com.sonalake.salarycalculator.common

import groovyx.net.http.EncoderRegistry
import groovyx.net.http.ParserRegistry
import groovyx.net.http.RESTClient
import org.codehaus.groovy.runtime.MethodClosure
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class WebSpecification extends Specification {

    @LocalServerPort
    int port

    RESTClient restClient

    def setup() {
        restClient = new RESTClient("http://localhost:${port}")
        restClient.handler.failure = { resp -> resp }
        EncoderRegistry encoderRegistry = restClient.getEncoder()
        encoderRegistry.putAt('application/hal+json', new MethodClosure(encoderRegistry, 'encodeJSON'))
        ParserRegistry parserRegistry = restClient.getParser()
        parserRegistry.putAt('application/hal+json', new MethodClosure(parserRegistry, 'parseJSON'))
    }
}
