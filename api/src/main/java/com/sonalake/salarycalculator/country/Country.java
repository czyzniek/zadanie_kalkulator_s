package com.sonalake.salarycalculator.country;

import com.sonalake.salarycalculator.common.jpa.BaseEntity;
import com.sonalake.salarycalculator.currency.Currency;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
public class Country extends BaseEntity {
    private String name;
    private Double tax;
    private Integer fixedCosts;
    @ManyToOne
    @JoinColumn(name = "currency_id")
    private Currency currency;
}
