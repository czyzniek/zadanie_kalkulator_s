package com.sonalake.salarycalculator.nbp;

import lombok.Data;

@Data
class NBPRate {
    private String currency;
    private String code;
    private Float mid;
}
