package com.sonalake.salarycalculator.nbp;

import com.sonalake.salarycalculator.currency.CurrencyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
class NBPExchangeRatesScheduler implements ApplicationListener<ApplicationReadyEvent> {

    private final NBPClient nbpClient = new NBPClient();
    private final CurrencyRepository currencyRepository;
    private final RateMapper rateMapper;

    @Autowired
    public NBPExchangeRatesScheduler(CurrencyRepository currencyRepository, RateMapper rateMapper) {
        this.currencyRepository = currencyRepository;
        this.rateMapper = rateMapper;
    }

    @Scheduled(cron = "0 0/5 * * * ?")
    private void downloadExchangeRatesFromNBP() {
        log.info("Start downloading exchange rates!");
        nbpClient.getExchangeRatesTables().stream()
                .flatMap(tableResponse -> tableResponse.getRates().stream())
                .map(rateMapper::rateResponseToCurrency)
                .forEach(currency -> {
                    currencyRepository.findByCodeIgnoreCase(currency.getCode())
                            .ifPresent(dbCurrency -> currency.setId(dbCurrency.getId()));
                    currencyRepository.save(currency);
                });
        log.info("Finished downloading exchange rates!");
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent applicationReadyEvent) {
        downloadExchangeRatesFromNBP();
    }
}
