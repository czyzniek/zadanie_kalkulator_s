package com.sonalake.salarycalculator.salary;

import com.sonalake.salarycalculator.country.Country;
import com.sonalake.salarycalculator.country.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
class SalaryService {
    
    private static final int DAYS_OF_MONTHS = 22;

    private final CountryRepository countryRepository;

    @Autowired
    SalaryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public double calculateSalaryInPLN(Double dailyRate, String currencyCode) {
        return countryRepository.findByCurrencyCodeIgnoreCase(currencyCode)
                .map(calculateSalaryInPln(dailyRate))
                .orElseThrow(() -> new IllegalArgumentException("Wrong currencyCode!"));
    }

    private Function<Country, Double> calculateSalaryInPln(Double dailyRate) {
        return country -> (dailyRate * DAYS_OF_MONTHS * (1.0f - country.getTax())) * country.getCurrency().getMid();
    }
}
